module Verigame.Player

public export
record PlayerStatus where
  constructor MkPlayerStatus
  defending : Bool

||| Represents a player, which has health and stamina.
public export
data Player : Nat -> Nat -> Type where
  MkPlayer : (name : String) -> (status : PlayerStatus) -> Player health stamina

%name Player player1, player2

export
healthOf : Player h s -> Nat
healthOf {h} _ = h

export
staminaOf : Player h s -> Nat
staminaOf {s} _ = s

export
nameOf : Player h s -> String
nameOf (MkPlayer n stat) = n

export
statusOf : Player h s -> PlayerStatus
statusOf (MkPlayer name status) = status

export
Show (Player h s) where
  show {h} {s} (MkPlayer name status) = "Player " ++ name ++ " (" ++ show h ++ " " ++ show s ++ ")"

export
defaultStatus : PlayerStatus
defaultStatus = MkPlayerStatus { defending = False }

export
newPlayer : String -> Player 100 100
newPlayer name = MkPlayer name defaultStatus
