module Verigame.Main

import Verigame.Game
import Verigame.Player

%default total

parseAction : String -> Maybe (cost : Nat ** Action cost)
parseAction s = case toLower s of
  "attack" => Just (_ ** Attack)
  "defend" => Just (_ ** Defend)
  "wait" => Just (_ ** Wait)
  _ => Nothing

partial
chooseAction : IO (cost : Nat ** Action cost)
chooseAction = do
  putStr "Enter an action: "
  choice <- getLine
  case parseAction choice of
       Nothing => do
         putStrLn (choice ++ " is not a valid action.")
         chooseAction
       (Just x) => pure x

partial
gameLoop : Game -> IO GameResult
gameLoop g@(AwaitInput self other) =
  do
    printLn g
    actionPrompt g
  where
    partial
    actionPrompt : Game -> IO GameResult
    actionPrompt game = do
      putStrLn $ (nameOf self) ++ ", what do you want to do?"
      (_ ** action) <- chooseAction
      case hasStamina action self of
           Yes prf =>
             let ((_ ** self'), (_ ** other')) = performAction action self other
             in if healthOf self' == 0 || healthOf other' == 0
                  then pure (Finished self' other')
                  else gameLoop (AwaitInput other' self')
           No contra => do
             putStrLn "Not enough stamina."
             actionPrompt game

partial
main : IO ()
main = do
  putStr "Player 1, what is your name? "
  name1 <- getLine
  putStr "Player 2, what is your name? "
  name2 <- getLine
  result <- gameLoop (newGame name1 name2)
  printLn result
