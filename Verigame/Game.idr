module Verigame.Game

import Verigame.Player

%default total

public export
data Game : Type where
     ||| Represents a game with two opposing players
     AwaitInput : (self : Player h1 s1) -> (other : Player h2 s2) -> Game

public export
data GameResult = Finished (Player h1 s1) (Player h2 s2)

export
newGame : String -> String -> Game
newGame name1 name2 = AwaitInput (newPlayer name1) (newPlayer name2)

export
Show Game where
  show (AwaitInput player1 player2) =
    show player1 ++ "\n" ++ show player2

export
Show GameResult where
  show (Finished player1 player2) =
    "Game finished: " ++ show player1 ++ ", " ++ show player2

public export
data Action : (cost : Nat) -> Type where
     Attack : Action 20
     Defend : Action 10
     Wait : Action 0

public export
actionCost : Action cost -> Nat
actionCost {cost} _ = cost

||| Proof that a player has enough stamina to execute an action.
export
data ActionPerformable : Action cost -> Player h s -> Type where
     AP : (action : Action cost) ->
          (p: Player h s) ->
          (ltePrf : LTE cost s) ->
          ActionPerformable action p

using (action : Action cost, player : Player h s)
  ||| Extract a proof that the action cost is less than a player's stamina.
  performableLTE : ActionPerformable action player -> LTE cost s
  performableLTE (AP _ _ ltePrf) = ltePrf

export
lteToPerformable : (action : Action cost) ->
                   (p: Player h s) ->
                   {auto ltePrf : LTE cost s} ->
                   ActionPerformable action p
lteToPerformable = AP

||| Provides a proof of whether a player has enough stamina to execute an action,
||| given the player and action.
export
hasStamina : (action : Action cost) ->
             (player : Player h s) ->
             Dec (ActionPerformable action player)
hasStamina {cost} {s} action player =
  case cost `isLTE` s of
       (Yes prf) => Yes (AP action player prf)
       (No contra) => No (\x => contra (performableLTE x))

||| Amount of damage dealt on an attack
attackDamage : Nat
attackDamage = 10

||| Return k - j, or 0 if j > k.
subtractClamped : Nat -> Nat -> Nat
subtractClamped k j = case j `isLTE` k of
                           (Yes prf) => k - j
                           (No contra) => 0

||| First player attacks the second player, returning the updated players
export
attack : Player h1 s1 ->
         Player h2 s2 ->
         {auto prf: (actionCost Attack) `LTE` s1} ->
         (Player h1 (s1 - (actionCost Attack)), (h3 : Nat ** Player h3 s2))
attack {h1} {s1} {h2} {s2} (MkPlayer n1 st1) (MkPlayer n2 st2) =
    if defending st2
      then doNothing
      else doAttack
  where attacker : Player h1 (s1 - (actionCost Attack))
        attacker = MkPlayer n1 st1

        updState : PlayerStatus
        updState = record { defending = False} st2

        doAttack = let remainingHp = subtractClamped h2 attackDamage
                       defender : Player remainingHp s2 = MkPlayer n2 updState
                   in (attacker, (_ ** defender))
        doNothing = let defender : Player h2 s2 = MkPlayer n2 updState
                    in (attacker, (_ ** defender))

||| First player defends
export
defend : Player h s ->
         {auto prf: (actionCost Defend) `LTE` s} ->
         Player h (s - (actionCost Defend))
defend (MkPlayer name status) =
  let status' = record { defending = True } status
  in MkPlayer name status'

||| Player recovers stamina
export
wait : Player h s -> Player h (min (s + 20) 100)
wait {s} (MkPlayer name status) = MkPlayer name status

public export
UpdatedPlayer : Type
UpdatedPlayer = (p : (Nat, Nat) ** Player (fst p) (snd p))

export
toUpdated : Player h s -> UpdatedPlayer
toUpdated (MkPlayer name status) = ((h, s) ** MkPlayer name status)

export
toUpdated2 : Player h1 s1 -> Player h2 s2 -> (UpdatedPlayer, UpdatedPlayer)
toUpdated2 p1 p2 = (toUpdated p1, toUpdated p2)

||| Applies the given action, returning the updated players.
||| @ action The action to perform.
||| @ self The player performing the action.
||| @ other The other player.
||| @ prf A proof that self has enough stamina to perform the action.
export
performAction : (action : Action cost) ->
                (self : Player hs ss) ->
                (other : Player ho so) ->
                {auto prf : ActionPerformable action self} ->
                (UpdatedPlayer, UpdatedPlayer)
performAction {prf} Attack self other =
  let ltePrf = performableLTE prf
      (self', (_ ** other')) = Verigame.Game.attack self other {prf = ltePrf}
  in toUpdated2 self' other'
performAction {prf} Defend self other =
  let ltePrf = performableLTE prf in toUpdated2 (defend self) other
performAction Wait self other = toUpdated2 (wait self) other
