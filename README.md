# Verigame

Verigame is a simple two-player, text-based game played on the command line. It
is written in Idris to learn how formal verification can be practically applied
to making software.

## Gameplay

The game is a two-player, text-based game played on the command line. Each
player begins with 100 health and 100 stamina points. The objective of the game
is to reduce the other player's health to 0. Each player alternates turns, and
may take one of the following actions each turn:

* Attack (costs 20 stamina): Reduces the other player's health by 20 points.
* Defend (costs 10 stamina): Nullifies the effect of the other player's next
  Attack action if it takes place on the next turn.
* Wait: Increases the player's stamina by 20 points, up to a total of 100.

## License

Copyright 2017 Bryan Tan

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
